#include "stdafx.h"
#include "Reports2Frame.h"
#include "Reports2View.h"
#include "Reports2.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static UINT indicators[] =
{
	ID_SEPARATOR           // status line indicator
};
const LPCTSTR RSTR_TB_PRINT = _T("Skrivut");
const LPCTSTR RSTR_TB_PDF = _T("Pdf");
const LPCTSTR RSTR_TB_MAIL = _T("Mail");
const LPCTSTR RSTR_TB_HALF_PAGE = _T("Halvsida");
const LPCTSTR RSTR_TB_WHOLE_PAGE = _T("Helsida");
const LPCTSTR RSTR_TB_GROUPS = _T("Sok");
const LPCTSTR RSTR_TB_EXPORT = _T("Export");

/////////////////////////////////////////////////////////////////////////////
// CReports2Frame

IMPLEMENT_DYNCREATE(CReports2Frame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CReports2Frame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CReports2Frame)
	//}}AFX_MSG_MAP
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_WM_MDIACTIVATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_MESSAGE(XTPWM_DOCKINGPANE_NOTIFY, OnDockingPaneNotify)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMsgSuite)
	ON_WM_SYSCOMMAND()
	ON_XTP_CREATECONTROL()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CReports2Frame construction/destruction

CReports2Frame::CReports2Frame()
{
	m_bOnce = TRUE;
	m_bOnCloseRunned = FALSE;	
}

CReports2Frame::~CReports2Frame()
{
}

BOOL CReports2Frame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CReports2Frame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE) // && m_bOkToClose)
		CMDIChildWnd::OnSysCommand(nID,lParam);
	
	if ((nID & 0xFFF0) == SC_MAXIMIZE || 
			(nID & 0xFFF0) == SC_MINIMIZE || 
			(nID & 0xFFF0) == SC_MOVE || 
			(nID & 0xFFF0) == SC_RESTORE || 
			(nID & 0xFFF0) == SC_SIZE)
		CMDIChildWnd::OnSysCommand(nID,lParam);
}

int CReports2Frame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	if (lpCreateControl->nID == ID_COMBO_ZOOMS)
	{
		CRect rCombo;
		if (!m_cbZooms.Create(WS_CHILD|WS_VISIBLE|CBS_DROPDOWNLIST|WS_CLIPCHILDREN,CRect(0,0,0,200), this, ID_COMBO_ZOOMS) )
		{
			AfxMessageBox(_T("ERROR:\nCMDIStandEntryFormFrame::OnCreateControl"));
		}
		else
		{
			m_cbZooms.SetOwner(this);
			GetWindowRect(&rCombo);
			m_cbZooms.SetLblFont(15,FW_NORMAL);     
			m_cbZooms.MoveWindow(45, 3, 100, 20);

			CXTPControlCustom * pCB = CXTPControlCustom::CreateControlCustom(&m_cbZooms);
			pCB->SetFlags(xtpFlagManualUpdate);

			lpCreateControl->buttonStyle = xtpButtonIconAndCaption;
			lpCreateControl->pControl = pCB;
		}

		return TRUE;
	}

	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// CReports2Frame diagnostics

#ifdef _DEBUG
void CReports2Frame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CReports2Frame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CReports2Frame message handlers

int CReports2Frame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;


	if (!m_wndStatusBar.Create(this) ||
			!m_wndStatusBar.SetIndicators(indicators,sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	m_bOnCloseRunned = FALSE;

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);

	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getProgDir() + _T("HMSIcons.icl");

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR1)
				{		
						pCtrl = p->GetAt(0);
						pCtrl->SetTooltip(g_pXML->str(32776));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 38);	// uppdatera
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						pCtrl = p->GetAt(1);
						pCtrl->SetTooltip(g_pXML->str(32771));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 36);	// RSTR_TB_PRINT
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						pCtrl = p->GetAt(2);
						pCtrl->SetTooltip(g_pXML->str(32772));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 9);	// Export
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						pCtrl = p->GetAt(3);
						pCtrl->SetTooltip(g_pXML->str(32775));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 27);	// MAIL
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						pCtrl = p->GetAt(4);
						pCtrl->SetTooltip(g_pXML->str(32774));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 15);	// RSTR_TB_HALF_PAGE
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						pCtrl = p->GetAt(5);
						pCtrl->SetTooltip(g_pXML->str(32773));
						hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 16);	// RSTR_TB_WHOLE_PAGE
						if (hIcon) pCtrl->SetCustomIcon(hIcon);
				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))

	CStringArray caStrings;
	caStrings.Add(_T("25%"));
	caStrings.Add(_T("50%"));
	caStrings.Add(_T("75%"));
	caStrings.Add(g_pXML->str(32774));
	caStrings.Add(g_pXML->str(32773));
	caStrings.Add(_T("150%"));
	caStrings.Add(_T("200%"));
	caStrings.Add(_T("300%"));
	m_cbZooms.SetItems(&caStrings);

	UpdateWindow();

	return 0;
}

void CReports2Frame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
	{
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
	}
}

void CReports2Frame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

LRESULT CReports2Frame::OnDockingPaneNotify(WPARAM wParam, LPARAM lParam)
{
	if (wParam == XTP_DPN_SHOWWINDOW)
	{
		return TRUE;
	}
	return FALSE;
}

void CReports2Frame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

    if(bShow && !IsWindowVisible() && m_bOnce == TRUE)
    {
		m_bOnce = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\Reports2\\Dialogs\\Report"), REG_ROOT);
		LoadPlacement(this, csBuf);
    }
}

LRESULT CReports2Frame::OnMsgSuite(WPARAM wParm, LPARAM lParm)
{
	// user pushed some buttons in the shell.
	switch(wParm)
	{
		case ID_NEW_ITEM:
		break;

		case ID_OPEN_ITEM:
		break;

		case ID_PREVIEW_ITEM:
		break;

		case ID_SAVE_ITEM:
		break;

		case ID_DELETE_ITEM:
		break;

		case ID_DBNAVIG_START:
			((CReports2View*)GetActiveView())->OnStep(wParm);
		break;

		case ID_DBNAVIG_NEXT:
			((CReports2View*)GetActiveView())->OnStep(wParm);
		break;

		case ID_DBNAVIG_PREV:
			((CReports2View*)GetActiveView())->OnStep(wParm);
		break;

		case ID_DBNAVIG_END:
			((CReports2View*)GetActiveView())->OnStep(wParm);
		break;
	};

	return 0;
}

void CReports2Frame::OnDestroy()
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\Reports2\\Dialogs\\Report"), REG_ROOT);
	SavePlacement(this, csBuf);
	m_bOnce = TRUE;

	CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();
}

void CReports2Frame::OnClose()
{
	if(m_bOnCloseRunned == TRUE) return;
	m_bOnCloseRunned = TRUE;

	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);

	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

void CReports2Frame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);

	CSize sz(0);
	if (m_wndToolBar.GetSafeHwnd())
	{
		RECT rect;
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, LM_HORZDOCK|LM_HORZ|LM_COMMIT);

		m_wndToolBar.MoveWindow(0, 0, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
	}
}
