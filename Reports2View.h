// Reports2View.h : interface of the CReports2View class
//

#pragma once

#include <afxwinforms.h>
#include "UC.h"
#include <msclr\event.h>
#include "Reports2Doc.h"

class CReports2View : public CWinFormsView
{
public:
	void OnPageChanged( System::Object^ sender, System::EventArgs^ e );
BEGIN_DELEGATE_MAP( CReports2View )
	EVENT_DELEGATE_ENTRY( OnPageChanged, System::Object^, System::EventArgs^ )
END_DELEGATE_MAP()

	CReports2View();
protected: // create from serialization only
	DECLARE_DYNCREATE(CReports2View)

	CString m_csTo;
	CString m_csSub;
	CString m_csText;

// Attributes
public:
	CReports2Doc* GetDocument() const;

// Operations
public:
	CString m_sReportFN;	// report to load
	CString m_sDocTitle;
	CString m_sTextFileName;
	CString m_sArguments;
	int m_action;
	int m_nWinAuth;

	TCHAR m_szDBServerPath[MAX_PATH];
	TCHAR m_szDBUser[32];
	TCHAR m_szDBPsw[32];
	TCHAR m_szDBDSNName[128];
	TCHAR m_szDBLocation[128];
	TCHAR m_szDBName[128];
	SAClient_t m_saClient;

	int m_nZoom;	// zoom in viewer

// Overrides
public:
//	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	BOOL m_bOnce;
	BOOL m_bIsShowing;

// Implementation
public:
	void showReport();
	virtual void OnInitialUpdate(); // called first time after construct
	virtual ~CReports2View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

//protected:
	gcroot<Reports2::UC ^> m_ViewControl;

// Generated message map functions
	afx_msg void OnStep(int nID);
	afx_msg void OnSetStatusbar(int nCurrent);
protected:
	afx_msg BOOL PreTranslateMessage(MSG *pMsg);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	DECLARE_MESSAGE_MAP()

	void OnReportPrint();
	void OnReportExport();
	void OnZoomWholePage();
	void OnZoomPageWidth();
	void OnSendEmail();
	void OnZoomChange();
	void OnDoRefresh();
	void LoadArguments();

public:
	LPCTSTR getReportTitle(void)
	{
		return _T("");
	}

	void setReportData(LPCTSTR fn,LPCTSTR text_file,LPCTSTR doc_title)
	{
		m_sReportFN = fn;
		m_sDocTitle = doc_title;
		m_sTextFileName = text_file;
		m_sArguments = _T("");
		m_action = DB_FILE;
	}
	void setReportData(LPCTSTR cap,_user_msg& msg,enumAction action)
	{
		m_sDocTitle = cap;
		m_sReportFN = msg.getName();
		m_sTextFileName = msg.getFileName();
		if(msg.getVoidBuf() != NULL)
		{
			m_sArguments = ((CString*)msg.getVoidBuf())->GetBuffer();
		}
		else
		{
			m_sArguments = msg.getArgStr();
		}
		m_action = action;
	}

	void PreCreateReport();
};

#ifndef _DEBUG  // debug version in Reports2View.cpp
inline CReports2Doc* CReports2View::GetDocument() const
   { return reinterpret_cast<CReports2Doc*>(m_pDocument); }
#endif
