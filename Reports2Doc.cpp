// Reports2Doc.cpp : implementation file
//

#include "stdafx.h"
#include "Reports2.h"
#include "Reports2Doc.h"


// CReports2Doc

IMPLEMENT_DYNCREATE(CReports2Doc, CDocument)

CReports2Doc::CReports2Doc()
{
}

BOOL CReports2Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	return TRUE;
}

CReports2Doc::~CReports2Doc()
{
}


BEGIN_MESSAGE_MAP(CReports2Doc, CDocument)
END_MESSAGE_MAP()


// CReports2Doc diagnostics

#ifdef _DEBUG
void CReports2Doc::AssertValid() const
{
	CDocument::AssertValid();
}

#ifndef _WIN32_WCE
void CReports2Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif
#endif //_DEBUG

#ifndef _WIN32_WCE
// CReports2Doc serialization

void CReports2Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}
#endif


// CReports2Doc commands
