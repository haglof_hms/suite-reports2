// Reports2.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

RLFReader* g_pXML;
CWinApp* pApp = AfxGetApp();
HINSTANCE hInst = NULL;

static AFX_EXTENSION_MODULE Reports2DLL = { NULL, NULL };

#ifdef _MANAGED
#pragma managed(push, off)
#endif

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);
	
	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("Reports2.DLL Initializing!\n");
		
		g_pXML = new RLFReader();	// create the XML-parser class.

		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(Reports2DLL, hInstance))
			return 0;

		new CDynLinkLibrary(Reports2DLL);
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("Reports2.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(Reports2DLL);

		delete g_pXML;
	}

	hInst = hInstance;

	return 1;   // ok
}

#ifdef _MANAGED
#pragma managed(pop)
#endif
