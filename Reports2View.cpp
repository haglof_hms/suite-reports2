// Reports2View.cpp : implementation of the CReports2View class
//

#include "stdafx.h"
#include "Reports2.h"
#include "Reports2Doc.h"
#include "Reports2View.h"
#include "Reports2Frame.h"
#include "UC.h"
#include <mapi.h>
#include "IMapi.h"
#include "resource.h"
#include <msclr\event.h>

using namespace System;
using namespace System::Windows::Forms;
using namespace CrystalDecisions::Shared;
using namespace CrystalDecisions::CrystalReports::Engine;
using namespace CrystalDecisions::Windows::Forms;
using namespace CrystalDecisions::ReportSource;
using namespace System::Drawing::Printing;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

int SplitString2(const CString& input, const CString& delimiter, CStringArray& caValues, CStringArray& caVars)
{
	int nPosStart=0, nPosDel=-1, nPosEnd=-1;
	int nLoop, nLen=0, nNumFound=0;
	CString csBuf;

	nLen = input.GetLength();
	caValues.RemoveAll();
	caVars.RemoveAll();

	for(nLoop=0; nLoop<nLen; nLoop++)
	{
		if(input.GetAt(nLoop) == delimiter)
		{
			nPosEnd = nLoop;

			if(nPosDel < nPosStart || nPosDel > nPosEnd)
			{
				nNumFound++;
				csBuf.Format(_T("arg%d"), nNumFound);
				caVars.Add(csBuf);

				// copy value
				if(nPosStart == nPosEnd)
					caValues.Add(_T(""));
				else if(nPosStart == 0)
					caValues.Add( input.Mid(nPosStart, nPosEnd) );
				else
					caValues.Add( input.Mid(nPosStart, nPosEnd-nPosStart) );
			}
			else
			{
				// copy value
				if(nPosStart == nPosEnd)
					caValues.Add(_T(""));
				else
					caValues.Add( input.Mid(nPosDel+1, nPosEnd-nPosDel-1) );
			}

			nPosStart = nLoop+1;
		}
		else if(input.GetAt(nLoop) == '=')
		{
			nPosDel = nLoop;

			// copy argument name
			if(nPosStart == 0)
				caVars.Add( input.Mid(nPosStart, nPosDel) );
			else
				caVars.Add( input.Mid(nPosStart, nPosDel-nPosStart) );

			nPosStart = nLoop;
		}
	}

	return caVars.GetSize();
}


// CReports2View
IMPLEMENT_DYNCREATE(CReports2View, CWinFormsView)

BEGIN_MESSAGE_MAP(CReports2View, CWinFormsView)
	//{{AFX_MSG_MAP(CReports2View)
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_COMMAND(ID_TBTN_PRINTOUT, OnReportPrint)
	ON_COMMAND(ID_TBTN_CREATEPDF, OnReportExport)
	ON_COMMAND(ID_TBTN_WHOLEPAGE, OnZoomWholePage)
	ON_COMMAND(ID_TBTN_HALFPAGE, OnZoomPageWidth)
	ON_COMMAND(ID_TBTN_SENDMAIL, OnSendEmail)
	ON_COMMAND(ID_TBTN_REFRESH, OnDoRefresh)
	//}}AFX_MSG_MAP
	ON_CONTROL(CBN_SELCHANGE, ID_COMBO_ZOOMS, OnZoomChange)
END_MESSAGE_MAP()

//COleControlSite

// CReports2View construction/destruction
CReports2View::CReports2View(): CWinFormsView(Reports2::UC::typeid)
{
	m_bOnce = FALSE;
	m_bIsShowing = FALSE;

	m_csTo = _T("");
	m_csSub = _T("");
	m_csText = _T("");
}

CReports2View::~CReports2View()
{
}

BOOL CReports2View::PreTranslateMessage(MSG *pMsg)
{
	if (pMsg->message == WM_KEYUP)
	{
		if (pMsg->wParam == VK_ESCAPE)
		{
			return FALSE;
		}
	}

	return CWinFormsView::PreTranslateMessage(pMsg);
}

// CReports2View diagnostics

#ifdef _DEBUG
void CReports2View::AssertValid() const
{
	CWinFormsView::AssertValid();
}

void CReports2View::Dump(CDumpContext& dc) const
{
	CWinFormsView::Dump(dc);
}

CReports2Doc* CReports2View::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CReports2Doc)));
	return (CReports2Doc*)m_pDocument;
}
#endif //_DEBUG

void CReports2View::OnClose()
{
	CWinFormsView::OnClose();
}

void CReports2View::OnDestroy()
{
	if(m_ViewControl->reportDocument1->IsLoaded)
	{
		m_ViewControl->reportDocument1->Close();
	}

	m_ViewControl->~UC();	// get rid of the .net controls
	GC::Collect();

	CWinFormsView::OnDestroy();
}

void CReports2View::OnSize(UINT nType, int cx, int cy)
{
	CWinFormsView::OnSize(nType, cx, cy);
}

void CReports2View::OnSetFocus(CWnd* pOldWnd)
{
	CWinFormsView::OnSetFocus(pOldWnd);

	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_LIST, FALSE);

	OnSetStatusbar(m_ViewControl->crystalReportViewer1->GetCurrentPageNumber());
}


// CReports2View message handlers
void CReports2View::OnInitialUpdate()
{
	CWinFormsView::OnInitialUpdate();

	// Get information on Database server used; 080107 p�d
	TCHAR szBuf[256];
	getDBUserInfo(szBuf, m_szDBName, m_szDBUser, m_szDBPsw, &m_saClient, &m_nWinAuth);
	int nLen = _tcslen(szBuf);
	for(int nLoop=0; nLoop<nLen; nLoop++)
	{
		if(szBuf[nLoop] == '@')
		{
			_tcsncpy_s(m_szDBLocation, nLoop+1, szBuf, _TRUNCATE);
			break;
		}
	}


	try
	{
		m_ViewControl = safe_cast<Reports2::UC ^>(this->GetControl());
		m_ViewControl->crystalReportViewer1->ShowLogo = FALSE;
		m_ViewControl->crystalReportViewer1->PageChanged += MAKE_DELEGATE( System::EventHandler, OnPageChanged );
	}
	catch(SystemException ^e)
	{
		AfxMessageBox( CString(e->Message) );
	}
}


void CReports2View::showReport(void)
{
	// No need to go on if file doesn't exixts; 080107 p�d
	if (!fileExists(m_sReportFN)) return;


	// load the report-design
	String ^sReportFN = gcnew String(m_sReportFN);
	m_ViewControl->reportDocument1->Load(sReportFN);

	LoadArguments();

	//Print the Report to window
	m_ViewControl->crystalReportViewer1->ReportSource = m_ViewControl->reportDocument1;

	// Set zoom
	m_nZoom = regGetInt(REG_ROOT, _T("Reports2"), _T("Zoom"), 1);
	m_ViewControl->crystalReportViewer1->Zoom(m_nZoom); // Show Whole page

	CComboBox* pBox = (CComboBox*)GetParentFrame()->GetDlgItem(ID_COMBO_ZOOMS);
	if(pBox)
	{
		int nIndex = 0;
		if(m_nZoom == 25)	// 25%
		{
			nIndex = 0;
		}
		else if(m_nZoom == 50)	// 50%
		{
			nIndex = 1;
		}
		else if(m_nZoom == 75)	// 75%
		{
			nIndex = 2;
		}
		else if(m_nZoom == 1)	// 100%
		{
			nIndex = 3;
		}
		else if(m_nZoom == 2)	// visa hel sida
		{
			nIndex = 4;
		}
		else if(m_nZoom == 150)	// 150%
		{
			nIndex = 5;
		}
		else if(m_nZoom == 200)	// 200%
		{
			nIndex = 6;
		}
		else if(m_nZoom == 300)	// 300%
		{
			nIndex = 7;
		}

		pBox->SetCurSel(nIndex);
	}

	m_bIsShowing = TRUE;

	m_ViewControl->crystalReportViewer1->Cursor = System::Windows::Forms::Cursors::Default;
}


void CReports2View::OnReportPrint()
{
	PrintDialog ^pd = gcnew PrintDialog();
	pd->AllowCurrentPage = true;
	pd->AllowSelection = true;
	pd->AllowSomePages = true;
	pd->UseEXDialog = true;
	pd->ShowNetwork = true;
	pd->AllowPrintToFile = true;

	PrintDocument ^pdoc = gcnew PrintDocument();
	pd->Document = pdoc;

	if(pd->ShowDialog() == DialogResult::OK)
	{
		try
		{
			PageMargins pageMargins = m_ViewControl->reportDocument1->PrintOptions->PageMargins;
			m_ViewControl->reportDocument1->PrintOptions->CopyFrom(pd->PrinterSettings, pd->PrinterSettings->DefaultPageSettings);
			m_ViewControl->reportDocument1->PrintOptions->PrinterName = pd->PrinterSettings->PrinterName;
			m_ViewControl->reportDocument1->PrintOptions->ApplyPageMargins(pageMargins);
			m_ViewControl->reportDocument1->PrintToPrinter(pd->PrinterSettings->Copies, pd->PrinterSettings->Collate, pd->PrinterSettings->FromPage, pd->PrinterSettings->ToPage);
		}
		catch(Exception^ engEx)
		{
			AfxMessageBox( CString(engEx->Message) );
		}
	}
}

void CReports2View::OnReportExport()
{
	// display a popup-menu with the different alternatives (documents).
	POINT point;
	CMenu menuPopup;
	menuPopup.CreatePopupMenu();

	// add the files to the popup-menu
	menuPopup.AppendMenu(MF_STRING, 1, g_pXML->str(106));	// PDF
	menuPopup.AppendMenu(MF_STRING, 2, g_pXML->str(107));	// RTF
	menuPopup.AppendMenu(MF_STRING, 3, g_pXML->str(108));	// Excel
    menuPopup.AppendMenu(MF_STRING, 4, g_pXML->str(109));	// Excel

	GetCursorPos(&point);
	int nRet = ::TrackPopupMenu(menuPopup.GetSafeHmenu(), TPM_NONOTIFY|TPM_RETURNCMD|TPM_LEFTALIGN|TPM_RIGHTBUTTON, point.x, point.y, NULL, this->GetSafeHwnd(), NULL);
	menuPopup.DestroyMenu();
	if(nRet == -1) return;

	if(nRet == 1)	// PDF
	{
		CFileDialog *dlg = new CFileDialog( 
			FALSE,				// File save dialog
			_T("pdf"),
			_T(""),
			OFN_HIDEREADONLY|OFN_FILEMUSTEXIST|OFN_EXPLORER,
			_T("pdf (*.pdf)|*.pdf|"), 
			NULL 
			);
		if( dlg->DoModal() == IDOK )
		{
			String ^strPath = gcnew String(dlg->GetPathName());
			m_ViewControl->reportDocument1->ExportToDisk(ExportFormatType::PortableDocFormat, strPath);
		}	// if( dlg.DoModal() == IDOK )
		delete dlg;
	}
	else if(nRet == 2)	// RTF
	{
		CFileDialog *dlg = new CFileDialog( 
			FALSE,				// File save dialog
			_T("rtf"),
			_T(""),
			OFN_HIDEREADONLY|OFN_FILEMUSTEXIST|OFN_EXPLORER,
			_T("rtf (*.rtf)|*.rtf|"), 
			NULL 
			);
		if( dlg->DoModal() == IDOK )
		{
			String ^strPath = gcnew String(dlg->GetPathName());
			m_ViewControl->reportDocument1->ExportToDisk(ExportFormatType::RichText, strPath);
		}	// if( dlg.DoModal() == IDOK )
		delete dlg;
	}
	else if(nRet == 3)	// Excel
	{
		CFileDialog *dlg = new CFileDialog( 
			FALSE,				// File save dialog
			_T("xls"),
			_T(""),
			OFN_HIDEREADONLY|OFN_FILEMUSTEXIST|OFN_EXPLORER,
			_T("xls (*.xls)|*.xls|"), 
			NULL 
			);
		if( dlg->DoModal() == IDOK )
		{
			String ^strPath = gcnew String(dlg->GetPathName());
			m_ViewControl->reportDocument1->ExportToDisk(ExportFormatType::Excel, strPath);
		}	// if( dlg.DoModal() == IDOK )
		delete dlg;
	}
	else if (nRet == 4) //CSV
	{
		//CrystalDecisions::Shared::CharacterSeparatedValuesFormatOptions csvExpOpts = new CrystalDecisions::Shared::CharacterSeparatedValuesFormatOptions();
		CFileDialog *dlg = new CFileDialog( 
			FALSE,				// File save dialog
			_T("csv"),
			_T(""),
			OFN_HIDEREADONLY|OFN_FILEMUSTEXIST|OFN_EXPLORER,
			_T("csv (*.csv)|*.csv|"), 
			NULL 
			);
		if( dlg->DoModal() == IDOK ) //Added method for csv export. 20121126 J� #3496
		{
		
			//Get filepath
			String ^strPath = gcnew String(dlg->GetPathName());
			//Retreive exportoptions
			ExportOptions ^exportOpts=m_ViewControl->reportDocument1->ExportOptions;
			//Set to csv type
			exportOpts->ExportFormatType = ExportFormatType::CharacterSeparatedValues;
			DiskFileDestinationOptions ^diskOpts = ExportOptions::CreateDiskFileDestinationOptions();
			CharacterSeparatedValuesFormatOptions ^csvFormat = ExportOptions::CreateCharacterSeparatedValuesFormatOptions();
			//Set options for csv export			
			csvFormat->GroupSectionsOption = CsvExportSectionsOption::ExportIsolated;
			csvFormat->SeparatorText=_T(";");
			csvFormat->ExportMode= CsvExportMode::Standard;
			exportOpts->ExportFormatOptions=csvFormat;
			exportOpts->ExportDestinationType = ExportDestinationType::DiskFile;
			diskOpts->DiskFileName=strPath;
    		exportOpts->ExportDestinationOptions = diskOpts;
			m_ViewControl->reportDocument1->Export();
			//m_ViewControl->reportDocument1->ExportToDisk(ExportFormatType::CharacterSeparatedValues, strPath);			

		}	// if( dlg.DoModal() == IDOK )
		delete dlg;
	}
}

void CReports2View::OnZoomWholePage()
{
	m_ViewControl->crystalReportViewer1->Zoom(1);

	// save this setting to registrty
	m_nZoom = 1;
	regSetInt(REG_ROOT, _T("Reports2"), _T("Zoom"), m_nZoom);
}

void CReports2View::OnZoomPageWidth()
{
	m_ViewControl->crystalReportViewer1->Zoom(2);

	// save this setting to registrty
	m_nZoom = 2;
	regSetInt(REG_ROOT, _T("Reports2"), _T("Zoom"), m_nZoom);
}

// attach current report in a email
#define BUFSIZE 4096
void CReports2View::OnSendEmail()
{
	// send the report
	CIMapi mail;
	if (mail.Error() == 0) //IMAPI_SUCCESS)
	{
		if(!m_csTo.IsEmpty()) mail.To(m_csTo);
		if(!m_csSub.IsEmpty()) mail.Subject(m_csSub);
		if(!m_csText.IsEmpty()) mail.Text(m_csText);

		DWORD dwBufSize=BUFSIZE;
		TCHAR lpPathBuffer[BUFSIZE];
		GetTempPath(dwBufSize, lpPathBuffer);	// get the temporary path
		CString csTempPath;

		CString csAtt = _T("email_attach.pdf");
		csTempPath.Format(_T("%s%s"), lpPathBuffer, csAtt);

		// create a PDF
		String ^strPath = gcnew String(csTempPath);
		m_ViewControl->reportDocument1->ExportToDisk(ExportFormatType::PortableDocFormat, strPath);
		mail.Attach(csTempPath);
		mail.Send();
	}
	else
	{
		// Do something appropriate to the error...
		AfxMessageBox(g_pXML->str(103)); //_T("No email-client installed!"));
	}
}

void CReports2View::OnPageChanged( System::Object^ sender, System::EventArgs^ e )
{
	OnSetStatusbar(m_ViewControl->crystalReportViewer1->GetCurrentPageNumber());
}

void CReports2View::OnSetStatusbar(int nCurrent)
{
	// set the statusbar text
	CString csBuf;
	int nMax;

	if(!m_ViewControl->reportDocument1->IsLoaded)
		return;

	try
	{
		CrystalDecisions::Shared::ReportPageRequestContext^ req;
		req = gcnew CrystalDecisions::Shared::ReportPageRequestContext();
		nMax = m_ViewControl->reportDocument1->FormatEngine->GetLastPageNumber(req);
	}
	catch(Exception^ /*engEx*/)
	{
		return;
	}


	// page x/y
	if(nCurrent > nMax)
		csBuf.Format(_T("%s %d/%d"), g_pXML->str(104), nMax, nMax);
	else if(nCurrent <= 0)
		csBuf.Format(_T("%s 1/%d"), g_pXML->str(104), nMax);
	else
		csBuf.Format(_T("%s %d/%d"), g_pXML->str(104), nCurrent, nMax);

	((CReports2Frame*)GetParentFrame())->m_wndStatusBar.SetPaneText(0, csBuf);


	// toggle the navigation-buttons aswell
	if(nCurrent <= 1)	// first place
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, TRUE);
	}

	if(nCurrent >= nMax)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, TRUE);
	}
}

void CReports2View::OnStep(int nID)
{
	int nPage = m_ViewControl->crystalReportViewer1->GetCurrentPageNumber();

	switch(nID)
	{
		case ID_DBNAVIG_START:
			m_ViewControl->crystalReportViewer1->ShowFirstPage();
		break;

		case ID_DBNAVIG_PREV:
			if(nPage != 0)
			{
				nPage--;
				m_ViewControl->crystalReportViewer1->ShowNthPage(nPage);
			}
		break;

		case ID_DBNAVIG_NEXT:
			if(nPage != 0)
			{
				nPage++;
				m_ViewControl->crystalReportViewer1->ShowNthPage(nPage);
			}
		break;

		case ID_DBNAVIG_END:
			m_ViewControl->crystalReportViewer1->ShowLastPage();
		break;
	};
}

void CReports2View::OnZoomChange()
{
	CComboBox* pBox = (CComboBox*)GetParentFrame()->GetDlgItem(ID_COMBO_ZOOMS);
	if(!pBox) return;

	int nSelect = pBox->GetCurSel();
	if(nSelect == 0)	// 25%
	{
		m_nZoom = 25;
	}
	else if(nSelect == 1)	// 50%
	{
		m_nZoom = 50;
	}
	else if(nSelect == 2)	// 75%
	{
		m_nZoom = 75;
	}
	else if(nSelect == 3)	// 100%
	{
		m_nZoom = 1;
	}
	else if(nSelect == 4)	// visa hel sida
	{
		m_nZoom = 2;
	}
	else if(nSelect == 5)	// 150%
	{
		m_nZoom = 150;
	}
	else if(nSelect == 6)	// 200%
	{
		m_nZoom = 200;
	}
	else if(nSelect == 7)	// 300%
	{
		m_nZoom = 300;
	}
	else
	{
		m_nZoom = 2;
	}

	m_ViewControl->crystalReportViewer1->Zoom(m_nZoom);

	// save this setting to registry
	regSetInt(REG_ROOT, _T("Reports2"), _T("Zoom"), m_nZoom);
}

void CReports2View::OnDoRefresh()
{
	m_ViewControl->reportDocument1->Refresh();
	LoadArguments();
	m_ViewControl->crystalReportViewer1->ReportSource = m_ViewControl->reportDocument1;
	m_ViewControl->crystalReportViewer1->Zoom(m_nZoom); // Show Whole page
}

void CReports2View::PreCreateReport()
{
	CrystalDecisions::CrystalReports::Engine::ReportDocument^ reportDocument1;
	reportDocument1 = (gcnew CrystalDecisions::CrystalReports::Engine::ReportDocument());
}

void CReports2View::LoadArguments()
{
	BOOL bIsArguments = FALSE;
	CStringArray arguments;
	CStringArray variables;
	CString sArgName;
	TCHAR szValue[255];

	if (!m_sArguments.IsEmpty())
	{
		// Make sure the last character in argumant string is a semicolon; 070207 p�d
		if (m_sArguments.Right(1) != ";")
		{
			m_sArguments += ";";
		}

		// Check out if there is arguments; 070207 p�d
		bIsArguments = (SplitString2(m_sArguments, _T(";"), arguments, variables) > 0);
	}


	String ^sDBLocation = gcnew String(m_szDBLocation);
	String ^sDBName = gcnew String(m_szDBName);
	String ^sDBUser = gcnew String(m_szDBUser);
	String ^sDBPsw = gcnew String(m_szDBPsw);

	BOOL bUseXml = FALSE;
	String ^strXMLFile;
	String ^strXSDFile;


	// Setup and add argumants to report; 070207 p�d
	if (bIsArguments)
	{
		String ^strValue;
		String ^strArg;
		CrystalDecisions::Shared::ParameterDiscreteValue ^crParameterDiscreteValue = gcnew CrystalDecisions::Shared::ParameterDiscreteValue();
		CrystalDecisions::Shared::ParameterValues ^crParameterValues = gcnew CrystalDecisions::Shared::ParameterValues();

		for (int i = 0;i < variables.GetCount();i++)
		{
			_stprintf(szValue, _T("%s"), arguments.GetAt(i));

			if(variables.GetAt(i) == _T("email_to"))
				m_csTo = szValue;
			else if(variables.GetAt(i) == _T("email_subject"))
				m_csSub = szValue;
			else if(variables.GetAt(i) == _T("SourceXML"))
			{
				bUseXml = TRUE;
				strXMLFile = gcnew String(szValue);
			}
			else if(variables.GetAt(i) == _T("SourceXSD"))
			{
				bUseXml = TRUE;
				strXSDFile = gcnew String(szValue);
			}
			else if(szValue != _T(""))
			{
				try
				{
					strValue = gcnew String(szValue);
					strArg = gcnew String(variables.GetAt(i));

					// Set the first parameter
					// - Get the parameter, tell it to use the current values vs default value.
					// - Tell it the parameter contains 1 discrete value vs multiple values.
					// - Set the parameter's value.
					// - Add it and apply it.
					// - Repeat these statements for each parameter.
					//
					crParameterValues = m_ViewControl->reportDocument1->DataDefinition->ParameterFields[strArg]->CurrentValues;
					crParameterDiscreteValue = gcnew CrystalDecisions::Shared::ParameterDiscreteValue();
					crParameterDiscreteValue->Value = strValue->ToString();
					crParameterValues->Add(crParameterDiscreteValue);
					m_ViewControl->reportDocument1->DataDefinition->ParameterFields[strArg]->ApplyCurrentValues(crParameterValues);
				}
				catch(...)
				{
					AfxMessageBox(_T("Error adding argument to report!"));
				}
			}

		}
	}	// if (bIsArguments)


	// are we going to use XML as datasource?
	if(bUseXml == TRUE)
	{
		DataSet ^dataSet = gcnew DataSet();
		dataSet->ReadXmlSchema(strXSDFile);
		dataSet->ReadXml(strXMLFile, XmlReadMode::IgnoreSchema);
		m_ViewControl->reportDocument1->SetDataSource(dataSet);
	}
	else
	{
		try
		{
			// set the database source
			if(m_nWinAuth == 0)	// use windows auth
			{
				m_ViewControl->reportDocument1->SetDatabaseLogon("", "", sDBLocation, sDBName, true);
			}
			else
			{
				m_ViewControl->reportDocument1->SetDatabaseLogon(sDBUser, sDBPsw, sDBLocation, sDBName, true);
			}

			if (m_ViewControl->reportDocument1->Database->Tables->Count > 0)
			{
				CrystalDecisions::Shared::TableLogOnInfo ^logOnInfo = gcnew CrystalDecisions::Shared::TableLogOnInfo();
				for (int i = 0; i < m_ViewControl->reportDocument1->Database->Tables->Count; i++)
				{
					logOnInfo = m_ViewControl->reportDocument1->Database->Tables[i]->LogOnInfo;
					logOnInfo->ConnectionInfo->ServerName = sDBLocation;
					logOnInfo->ConnectionInfo->DatabaseName = sDBName;
					if(m_nWinAuth == 0)	// use windows auth
					{
						logOnInfo->ConnectionInfo->IntegratedSecurity = true;
						logOnInfo->ConnectionInfo->UserID = "";
						logOnInfo->ConnectionInfo->Password = "";
					}
					else	// use sql auth
					{
						logOnInfo->ConnectionInfo->IntegratedSecurity = false;
						logOnInfo->ConnectionInfo->UserID = sDBUser;
						logOnInfo->ConnectionInfo->Password = sDBPsw;
					}
					m_ViewControl->reportDocument1->Database->Tables[i]->ApplyLogOnInfo(logOnInfo);
				}
			}

		}
		catch(SystemException ^e)
		{
			AfxMessageBox( CString(e->Message) );
		}
	}
}