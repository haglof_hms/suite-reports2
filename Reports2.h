#pragma once

#ifndef _Reports2_H_
#define _Reports2_H_

#include <vector>

typedef std::vector<INDEX_TABLE> vecINDEX_TABLE;
typedef std::vector<INFO_TABLE> vecINFO_TABLE;

extern RLFReader* g_pXML;
extern HINSTANCE hInst;
extern CWinApp* pApp;


#define __BUILD
#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

class CReports2
{
public:
	CReports2();
	~CReports2();
};

extern CReports2 theApp;


#endif