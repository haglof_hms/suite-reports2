// Reports2Frame.h : interface of the CReports2Frame class
//

#pragma once

#include "CACBox.h"

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

class CReports2Frame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CReports2Frame)

	BOOL m_bOnce;
	BOOL m_bOnCloseRunned;

	CXTPToolBar m_wndToolBar;
	CXTPStatusBar m_wndStatusBar;
	CACBox m_cbZooms;	// combo for zoom

public:
	CReports2Frame();

// Attributes
public:
	CXTPDockingPaneManager m_paneManager;
	CXTPPropertyGrid m_wndPropertyGrid;

// Operations
public:
	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CReports2Frame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CReports2Frame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	LRESULT OnMsgSuite(WPARAM wParm, LPARAM lParm);

// Generated message map functions
protected:
	//{{AFX_MSG(CReports2Frame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg LRESULT OnDockingPaneNotify(WPARAM wParam, LPARAM lParam);
	afx_msg void OnPaint(void);
	afx_msg	void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg void OnZoomChange();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
};
