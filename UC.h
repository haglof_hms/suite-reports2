#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace CrystalDecisions::CrystalReports::Engine;
using namespace CrystalDecisions::Windows::Forms;
using namespace CrystalDecisions::Shared;
using namespace CrystalDecisions::ReportSource;
//using namespace Microsoft::VisualC::MFC;

namespace Reports2 {

	/// <summary>
	/// Summary for UC
	/// </summary>
	public ref class UC : public System::Windows::Forms::UserControl //,
	{
	public:
		UC(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~UC()
		{
			if (components)
			{
				delete components;
			}

			if(crystalReportViewer1)
				delete crystalReportViewer1;
			if(reportDocument1)
				delete reportDocument1;
		}
	public: CrystalDecisions::Windows::Forms::CrystalReportViewer^  crystalReportViewer1;
	public: CrystalDecisions::CrystalReports::Engine::ReportDocument^  reportDocument1;

	public: 

	public: 
	public: 
	protected: 

	protected: 

	protected: 

	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->crystalReportViewer1 = (gcnew CrystalDecisions::Windows::Forms::CrystalReportViewer());
			this->reportDocument1 = (gcnew CrystalDecisions::CrystalReports::Engine::ReportDocument());
			this->SuspendLayout();
			// 
			// crystalReportViewer1
			// 
			this->crystalReportViewer1->ActiveViewIndex = -1;
			this->crystalReportViewer1->AutoScroll = true;
			this->crystalReportViewer1->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->crystalReportViewer1->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->crystalReportViewer1->DisplayBackgroundEdge = false;
			this->crystalReportViewer1->DisplayStatusBar = false;
			this->crystalReportViewer1->DisplayToolbar = false;
			this->crystalReportViewer1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->crystalReportViewer1->EnableDrillDown = false;
			this->crystalReportViewer1->EnableToolTips = false;
			this->crystalReportViewer1->Location = System::Drawing::Point(0, 0);
			this->crystalReportViewer1->Name = L"crystalReportViewer1";
			this->crystalReportViewer1->ShowCloseButton = false;
			this->crystalReportViewer1->ShowExportButton = false;
			this->crystalReportViewer1->ShowGotoPageButton = false;
			this->crystalReportViewer1->ShowLogo = false;
			this->crystalReportViewer1->ShowPageNavigateButtons = false;
			this->crystalReportViewer1->ShowPrintButton = false;
			this->crystalReportViewer1->ShowRefreshButton = false;
			this->crystalReportViewer1->ShowTextSearchButton = false;
			this->crystalReportViewer1->ShowZoomButton = false;
			this->crystalReportViewer1->Size = System::Drawing::Size(394, 312);
			this->crystalReportViewer1->TabIndex = 0;
			this->crystalReportViewer1->ToolPanelView = CrystalDecisions::Windows::Forms::ToolPanelViewType::None;
			this->crystalReportViewer1->Load += gcnew System::EventHandler(this, &UC::crystalReportViewer1_Load);
			this->crystalReportViewer1->Enter += gcnew System::EventHandler(this, &UC::crystalReportViewer1_Enter);
			// 
			// UC
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->Controls->Add(this->crystalReportViewer1);
			this->Name = L"UC";
			this->Size = System::Drawing::Size(394, 312);
			this->ResumeLayout(false);

		}
#pragma endregion

	private: System::Void crystalReportViewer1_Load(System::Object^  sender, System::EventArgs^  e)
			 {
				 this->Cursor = System::Windows::Forms::Cursors::WaitCursor;
			 }
	private: System::Void crystalReportViewer1_Enter(System::Object^  sender, System::EventArgs^  e)
			 {
			 }
};
}
