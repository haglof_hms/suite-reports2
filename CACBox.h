/////////////////////////////////////////////////////////////////////////////
// CACBox; used in Toolbar for CMDIStandEntryFormFrame to hold
// e.g. Reports; 070323 p�d

class CACBox : public CComboBox
{
public:
	CACBox();

	void SetLblFont(int size,int weight,LPCTSTR font_name = _T("Arial"));
	void SetItems(CStringArray *caStrings);

protected:
	CFont *m_fnt1;
	CString m_sLangFN;

	CString getLangStr(int id);

	//{{AFX_MSG(CACBox)
	afx_msg void OnDestroy();
	afx_msg BOOL OnCBoxChange();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};
