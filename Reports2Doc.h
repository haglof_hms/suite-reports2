#pragma once

// CReports2Doc document

class CReports2Doc : public CDocument
{
	DECLARE_DYNCREATE(CReports2Doc)

public:
	CReports2Doc();
	virtual ~CReports2Doc();
#ifndef _WIN32_WCE
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual BOOL OnNewDocument();

	DECLARE_MESSAGE_MAP()
};
