#include "stdafx.h"
#include "Reports2.h"
#include "CACBox.h"

/////////////////////////////////////////////////////////////////////////////
// CACBox; used in Toolbar for CMDIStandEntryFormFrame to hold
// e.g. Reports; 070323 p�d

BEGIN_MESSAGE_MAP(CACBox, CComboBox)
	//{{AFX_MSG_MAP(CMyExtCBox)
	ON_WM_DESTROY()
//	ON_CONTROL_REFLECT_EX(CBN_SELCHANGE, OnCBoxChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CACBox::CACBox()
{
	m_fnt1 = new CFont();
}

void CACBox::OnDestroy()
{
	if (m_fnt1 != NULL)
		delete m_fnt1;
  
	CComboBox::OnDestroy();
}

void CACBox::SetItems(CStringArray *caStrings)
{
	for(int nLoop=0; nLoop<caStrings->GetSize(); nLoop++)
	{
		AddString(caStrings->GetAt(nLoop));
	}
}

BOOL CACBox::OnCBoxChange()
{
	return TRUE;
}

void CACBox::SetLblFont(int size,int weight,LPCTSTR font_name)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	lf.lfHeight = size;
	lf.lfWeight = weight;
	if (font_name != _T(""))
		_tcscpy(lf.lfFaceName, font_name);
	m_fnt1->CreateFontIndirect(&lf);

	SetFont(m_fnt1);
}

CString CACBox::getLangStr(int id)
{
	CString sStr;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sStr = xml->str(id);
		}
		delete xml;
	}
	return sStr;
}
