Suite Reports2

F�ljande m�ste vara installerat och inkluderat i Visual Studio Paths (Tools/Options/VC++ Directories:
* Microsoft Windows SDK for Visual Studio 2008

* Codejock Xtreme ToolkitPro (default location)
- Executeable files: C:\Program Files\Codejock Software\MFC\Xtreme ToolkitPro v11.2.1
- Include files: C:\Program Files\Codejock Software\MFC\Xtreme ToolkitPro v11.2.1\Source
- Library files: C:\Program Files\Codejock Software\MFC\Xtreme ToolkitPro v11.2.1\lib\vc90

SQLAPI++ (default location)
- Include files: C:\SQLAPI\include
- Libary files: C:\SQLAPI\lib

-----------------------------------------------------------------------------------------------------

F�ljande m�ste checkas ut var f�r sig i samma katalog d�r projektet ligger med de korrekta namnen:
Include (tex checkout https://hagsrv02/svn/code/PC/HMS/Include/trunk)
lib (tex checkout https://hagsrv02/svn/code/PC/HMS/lib/trunk)

Exempel:
workspace\Suite Reports2
workspace\Include
workspace\lib

OBS: TAG Projekt b�r checkas ut ist�llet ifall de ska vara "frysna i tiden" och inte f� in 
uppdateringar via "SVN Update" men samma namn som n�mns ovan m�ste anv�ndas p� den lokala 
h�rddisken f�r att slippa �ndra i Visual Studio projekt settings.



